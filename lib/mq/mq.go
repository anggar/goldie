package mq

import (
	"context"
	"log"

	"github.com/segmentio/kafka-go"
)

type MQ struct {
	Conn   *kafka.Conn
	Reader *kafka.Reader
}

func NewMQ(ctx context.Context, connUri string, topic string, partition int) *MQ {
	mq := MQ{}

	conn, err := kafka.DialLeader(ctx, "tcp", connUri, topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{connUri},
		Topic:     topic,
		Partition: partition,
		MinBytes:  10e3, // 10KB
		MaxBytes:  10e6, // 10MB
	})

	mq.Conn = conn
	mq.Reader = r

	return &mq
}

func (m *MQ) ConsumeParse(ctx context.Context, fn func([]byte)) {
	for {
		message, err := m.Reader.ReadMessage(ctx)
		if err != nil {
			break
		}

		fn(message.Value)
	}
}
