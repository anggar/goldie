package topupservice

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/segmentio/kafka-go"
	"github.com/teris-io/shortid"
	"gitlab.com/anggar/goldie/lib/db"
	"gitlab.com/anggar/goldie/lib/mq"
)

type TopupRequest struct {
	NoRekening string  `json:"norek"`
	Gram       float64 `json:"gram"`
	Harga      int     `json:"harga"`
}

type TopupRequestMQ struct {
	NoRekening   string  `json:"norek" db:"norek"`
	Gram         float64 `json:"gram" db:"gram"`
	HargaTopup   int     `json:"harga_topup" db:"harga_topup"`
	HargaBuyback int     `json:"harga_buyback" db:"harga_buyback"`
	Saldo        float64 `json:"saldo" db:"saldo"`
}

type TopupService struct {
	MQ *mq.MQ
	DB *db.DB
}

type Response struct {
	Error   bool   `json:"error"`
	Message string `json:"message,omitempty"`
	ReffId  string `json:"reff_id"`
}

func NewService(mqObj *mq.MQ, dbObj *db.DB) TopupService {
	return TopupService{
		MQ: mqObj,
		DB: dbObj,
	}
}

func (m TopupService) TopupHandler(w http.ResponseWriter, req *http.Request) {
	var (
		err            error
		currentPrice   TopupRequestMQ
		currentBalance float64
		reqBody        []byte
		reqBodyVal     TopupRequest
	)

	reqBody, err = ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.Unmarshal(reqBody, &reqBodyVal)

	err = m.DB.Get(&currentPrice, "SELECT harga_topup, harga_buyback FROM harga ORDER BY created_at DESC LIMIT 1")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = m.DB.Get(&currentBalance, "SELECT COALESCE(saldo, 0.0) FROM rekening WHERE id = $1", reqBodyVal.NoRekening)
	if err != sql.ErrNoRows && err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if reqBodyVal.Harga != currentPrice.HargaTopup {
		resp, _ := json.Marshal(Response{
			Error:   true,
			ReffId:  shortid.MustGenerate(),
			Message: "price is not same",
		})

		w.Write(resp)
		return
	}

	if strings.Split(strconv.FormatFloat(reqBodyVal.Gram*1000, 'f', 3, 64), ".")[1] != "000" {
		resp, _ := json.Marshal(Response{
			Error:   true,
			ReffId:  shortid.MustGenerate(),
			Message: "only allowed 3 digits precision purchase",
		})

		w.Write(resp)
		return
	}

	reqMQ, _ := json.Marshal(TopupRequestMQ{
		NoRekening:   reqBodyVal.NoRekening,
		Gram:         reqBodyVal.Gram,
		HargaTopup:   currentPrice.HargaTopup,
		HargaBuyback: currentPrice.HargaBuyback,
		Saldo:        currentBalance,
	})

	_, err = m.MQ.Conn.WriteMessages(
		kafka.Message{Value: reqMQ},
	)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, _ := json.Marshal(Response{
		ReffId: shortid.MustGenerate(),
	})
	w.Write(resp)
}
