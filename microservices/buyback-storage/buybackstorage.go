package buybackstorage

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"time"

	"github.com/teris-io/shortid"
	"gitlab.com/anggar/goldie/lib/db"
	"gitlab.com/anggar/goldie/lib/mq"
)

type BuybackRequestMQ struct {
	NoRekening   string  `json:"norek"`
	Gram         float64 `json:"gram"`
	HargaTopup   int     `json:"harga_topup"`
	HargaBuyback int     `json:"harga_buyback"`
	Saldo        float64 `json:"saldo"`
}

type BuybackStorage struct {
	MQ *mq.MQ
	DB *db.DB
}

type Response struct {
	Error   bool   `json:"error"`
	Message string `json:"message,omitempty"`
	ReffId  string `json:"reff_id"`
}

func NewStorage(mqObj *mq.MQ, dbObj *db.DB) BuybackStorage {
	return BuybackStorage{
		MQ: mqObj,
		DB: dbObj,
	}
}

func (m BuybackStorage) Consume(ctx context.Context) {
	m.MQ.ConsumeParse(ctx, func(a []byte) {
		var (
			reqBody        BuybackRequestMQ
			currentBalance float64
		)
		json.Unmarshal(a, &reqBody)

		if m.DB == nil {
			return
		}

		tx, err := m.DB.BeginTxx(ctx, nil)
		if err != nil {
			log.Println(err)
			return
		}
		defer tx.Rollback()

		err = m.DB.Get(&currentBalance, "SELECT COALESCE(saldo, 0.0) FROM rekening WHERE id = $1", reqBody.NoRekening)
		if err != sql.ErrNoRows && err != nil {
			return
		}

		query := `
		INSERT INTO transaksi (id, type, gram, saldo, harga_topup, harga_buyback, norek, created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
		`
		args := []any{
			shortid.MustGenerate(),
			"buyback",
			reqBody.Gram,
			currentBalance - reqBody.Gram,
			reqBody.HargaTopup,
			reqBody.HargaBuyback,
			reqBody.NoRekening,
			time.Now(),
		}

		_, err = tx.Exec(query, args...)
		if err != nil {
			log.Println(err)
			return
		}

		tx.Commit()

		tx, err = m.DB.BeginTxx(ctx, nil)
		if err != nil {
			log.Println(err)
			return
		}

		query = `
		INSERT INTO rekening (id, saldo)
		VALUES ($1, $2)
		ON CONFLICT (id) DO
			UPDATE SET saldo = rekening.saldo - $2
			WHERE rekening.id = $1
		`
		args = []any{
			reqBody.NoRekening,
			reqBody.Gram,
		}

		_, err = tx.Exec(query, args...)
		if err != nil {
			log.Println(err)
			return
		}

		tx.Commit()
	})
}
