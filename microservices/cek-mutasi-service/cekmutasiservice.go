package cekmutasiservice

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/teris-io/shortid"
	"gitlab.com/anggar/goldie/lib/db"
)

type CekMutasiRequest struct {
	NoRekening string `json:"norek"`
	StartDate  int64  `json:"start_date"`
	EndDate    int64  `json:"end_date"`
}

type Data struct {
	ID           string    `db:"id"`
	Type         string    `json:"type" db:"type"`
	NoRekening   string    `json:"norek" db:"norek"`
	Gram         float64   `json:"gram" db:"gram"`
	HargaTopup   int       `json:"harga_topup" db:"harga_topup"`
	HargaBuyback int       `json:"harga_buyback" db:"harga_buyback"`
	Saldo        float64   `json:"saldo" db:"saldo"`
	CreatedAt    time.Time `json:"created_at" db:"created_at"`
}

type Response struct {
	Error   bool   `json:"error"`
	ReffId  string `json:"reff_id,omitempty"`
	Message string `json:"message,omitempty"`
	Data    []Data `json:"data,omitempty"`
}

type CekMutasiService struct {
	DB *db.DB
}

func NewService(dbObj *db.DB) CekMutasiService {
	return CekMutasiService{
		DB: dbObj,
	}
}

func (m CekMutasiService) CekMutasiHandler(w http.ResponseWriter, req *http.Request) {
	var (
		err        error
		data       = make([]Data, 0)
		reqBody    []byte
		reqBodyVal CekMutasiRequest
	)

	reqBody, err = ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.Unmarshal(reqBody, &reqBodyVal)

	if err = m.DB.Select(
		&data,
		"SELECT * FROM transaksi WHERE created_at BETWEEN $1 AND $2 ORDER BY created_at DESC",
		time.Unix(reqBodyVal.StartDate, 0),
		time.Unix(reqBodyVal.EndDate, 0),
	); err != sql.ErrNoRows && err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err == sql.ErrNoRows {
		resp, _ := json.Marshal(Response{
			Error:   true,
			ReffId:  shortid.MustGenerate(),
			Message: "no data found",
		})
		w.Header().Set("Content-Type", "application/json")
		w.Write(resp)
		return
	}

	resp, _ := json.Marshal(Response{
		Data: data,
	})

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
