package cekdaldoservice

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/teris-io/shortid"
	"gitlab.com/anggar/goldie/lib/db"
)

type CekSaldoRequest struct {
	NoRekening string `json:"norek"`
}

type Data struct {
	NoRekening string  `json:"norek" db:"id"`
	Saldo      float64 `json:"saldo" db:"saldo"`
}

type Response struct {
	Error   bool   `json:"error"`
	ReffId  string `json:"reff_id,omitempty"`
	Message string `json:"message,omitempty"`
	Data    Data   `json:"data,omitempty"`
}

type CekSaldoService struct {
	DB *db.DB
}

func NewService(dbObj *db.DB) CekSaldoService {
	return CekSaldoService{
		DB: dbObj,
	}
}

func (m CekSaldoService) CekSaldoHandler(w http.ResponseWriter, req *http.Request) {
	var (
		err        error
		data       Data
		reqBody    []byte
		reqBodyVal CekSaldoRequest
	)

	reqBody, err = ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.Unmarshal(reqBody, &reqBodyVal)

	err = m.DB.Get(&data, "SELECT COALESCE(id, '') as id, COALESCE(saldo, 0) as saldo FROM rekening WHERE id = $1", reqBodyVal.NoRekening)
	if err != sql.ErrNoRows && err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err == sql.ErrNoRows {
		resp, _ := json.Marshal(Response{
			Error:   true,
			ReffId:  shortid.MustGenerate(),
			Message: "no data found",
		})
		w.Header().Set("Content-Type", "application/json")
		w.Write(resp)
		return
	}

	resp, _ := json.Marshal(Response{
		Data: data,
	})

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
