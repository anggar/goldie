package inputhargastorage

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/teris-io/shortid"
	"gitlab.com/anggar/goldie/lib/db"
	"gitlab.com/anggar/goldie/lib/mq"
)

type InputHargaRequest struct {
	AdminID      string `json:"admin_id"`
	HargaTopup   int    `json:"harga_topup"`
	HargaBuyback int    `json:"harga_buyback"`
}

type Response struct {
	Error   bool   `json:"error"`
	Message string `json:"message,omitempty"`
	ReffId  string `json:"reff_id"`
}

type InputHargaStorage struct {
	MQ *mq.MQ
	DB *db.DB
}

func NewStorage(mqObj *mq.MQ, dbObj *db.DB) InputHargaStorage {
	return InputHargaStorage{
		MQ: mqObj,
		DB: dbObj,
	}
}

func (m InputHargaStorage) Consume(ctx context.Context) {
	m.MQ.ConsumeParse(ctx, func(a []byte) {
		var reqBody InputHargaRequest
		json.Unmarshal(a, &reqBody)

		if m.DB == nil {
			return
		}

		tx, err := m.DB.BeginTxx(ctx, nil)
		if err != nil {
			log.Println(err)
			return
		}
		defer tx.Rollback()

		query := `
		INSERT INTO harga (id, harga_topup, harga_buyback, created_by, created_at)
		VALUES ($1, $2, $3, $4, $5)
		`
		args := []any{
			shortid.MustGenerate(),
			reqBody.HargaTopup,
			reqBody.HargaBuyback,
			reqBody.AdminID,
			time.Now(),
		}

		_, err = tx.Exec(query, args...)
		if err != nil {
			log.Println(err)
			return
		}

		tx.Commit()
	})
}
