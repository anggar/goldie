package inputhargaservice

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/segmentio/kafka-go"
	"github.com/teris-io/shortid"
	"gitlab.com/anggar/goldie/lib/mq"
)

type InputHargaRequest struct {
	AdminID      string `json:"admin_id"`
	HargaTopup   int    `json:"harga_topup"`
	HargaBuyback int    `json:"harga_buyback"`
}

type Response struct {
	Error   bool   `json:"error"`
	Message string `json:"message,omitempty"`
	ReffId  string `json:"reff_id"`
}

type InputHargaService struct {
	MQ *mq.MQ
}

func NewService(mqObj *mq.MQ) InputHargaService {
	return InputHargaService{
		MQ: mqObj,
	}
}

func (m InputHargaService) InputHargaHandler(w http.ResponseWriter, req *http.Request) {
	var (
		err     error
		reqBody []byte
	)

	reqBody, err = ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = m.MQ.Conn.WriteMessages(
		kafka.Message{Value: reqBody},
	)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, _ := json.Marshal(Response{
		ReffId: shortid.MustGenerate(),
	})

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
