package cekhargaservice

import (
	"encoding/json"
	"net/http"

	"gitlab.com/anggar/goldie/lib/db"
)

type CekHargaRequest struct {
	AdminID      string `json:"admin_id"`
	HargaTopup   int    `json:"harga_topup"`
	HargaBuyback int    `json:"harga_buyback"`
}

type Data struct {
	HargaTopup   int `json:"harga_topup" db:"harga_topup"`
	HargaBuyback int `json:"harga_buyback" db:"harga_buyback"`
}

type Response struct {
	Error bool `json:"error"`
	Data  Data `json:"data"`
}

type CekHargaService struct {
	DB *db.DB
}

func NewService(dbObj *db.DB) CekHargaService {
	return CekHargaService{
		DB: dbObj,
	}
}

func (m CekHargaService) CekHargaHandler(w http.ResponseWriter, req *http.Request) {
	var (
		err  error
		data Data
	)

	err = m.DB.Get(&data, "SELECT harga_topup, harga_buyback FROM harga ORDER BY created_at DESC LIMIT 1")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, _ := json.Marshal(Response{
		Data: data,
	})

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
