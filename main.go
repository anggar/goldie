package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload"

	"gitlab.com/anggar/goldie/lib/db"
	"gitlab.com/anggar/goldie/lib/mq"
	buybackService "gitlab.com/anggar/goldie/microservices/buyback-service"
	buybackStorage "gitlab.com/anggar/goldie/microservices/buyback-storage"
	cekHargaService "gitlab.com/anggar/goldie/microservices/cek-harga-service"
	cekMutasiService "gitlab.com/anggar/goldie/microservices/cek-mutasi-service"
	cekSaldoService "gitlab.com/anggar/goldie/microservices/cek-saldo-service"
	inputHargaService "gitlab.com/anggar/goldie/microservices/input-harga-service"
	inputHargaStorage "gitlab.com/anggar/goldie/microservices/input-harga-storage"
	topupService "gitlab.com/anggar/goldie/microservices/topup-service"
	topupStorage "gitlab.com/anggar/goldie/microservices/topup-storage"
)

type config struct {
	port  string
	dbURI string
	mqUri string
}

func main() {
	cfg := envConfig()

	dbConn, err := db.Open(cfg.dbURI)
	if err != nil {
		log.Fatalf("cannot open database: %v", err)
	}

	server := NewServer(context.Background(), dbConn, cfg.mqUri)
	server.Run(cfg.port)
	select {}
}

type Server struct {
	server *http.Server
	router *mux.Router
}

func NewServer(ctx context.Context, dbConn *db.DB, mqUri string) *Server {
	s := Server{
		server: &http.Server{
			WriteTimeout: 5 * time.Second,
			ReadTimeout:  5 * time.Second,
			IdleTimeout:  5 * time.Second,
		},
		router: mux.NewRouter(),
	}

	inputHargaMq := mq.NewMQ(ctx, mqUri, "input_harga", 0)
	inputHargaServiceClient := inputHargaService.NewService(inputHargaMq)
	inputHargaStorageClient := inputHargaStorage.NewStorage(inputHargaMq, dbConn)

	cekHargaServiceClient := cekHargaService.NewService(dbConn)

	topupMq := mq.NewMQ(ctx, mqUri, "topup", 0)
	topupServiceClient := topupService.NewService(topupMq, dbConn)
	topupStorageClient := topupStorage.NewStorage(topupMq, dbConn)

	cekSaldoServiceClient := cekSaldoService.NewService(dbConn)
	cekMutasiServiceClient := cekMutasiService.NewService(dbConn)

	buybackMq := mq.NewMQ(ctx, mqUri, "buyback", 0)
	buybackServiceClient := buybackService.NewService(buybackMq, dbConn)
	buybackStorageClient := buybackStorage.NewStorage(buybackMq, dbConn)

	apiRouter := s.router.PathPrefix("/api").Subrouter()
	apiRouter.HandleFunc("/input-harga/", inputHargaServiceClient.InputHargaHandler).Methods(http.MethodPost)
	apiRouter.HandleFunc("/check-harga/", cekHargaServiceClient.CekHargaHandler)
	apiRouter.HandleFunc("/topup/", topupServiceClient.TopupHandler).Methods(http.MethodPost)
	apiRouter.HandleFunc("/saldo/", cekSaldoServiceClient.CekSaldoHandler)
	apiRouter.HandleFunc("/mutasi/", cekMutasiServiceClient.CekMutasiHandler)
	apiRouter.HandleFunc("/buyback/", buybackServiceClient.BuybackHandler).Methods(http.MethodPost)

	go inputHargaStorageClient.Consume(ctx)
	go topupStorageClient.Consume(ctx)
	go buybackStorageClient.Consume(ctx)

	return &s
}

func (s *Server) Run(port string) error {
	if !strings.HasPrefix(port, ":") {
		port = ":" + port
	}
	s.server.Addr = port
	http.Handle("/", s.router)
	log.Printf("server starting on %s", port)
	return s.server.ListenAndServe()
}

func envConfig() config {
	port, ok := os.LookupEnv("PORT")
	if !ok {
		panic("PORT not provided")
	}

	dbURI, ok := os.LookupEnv("POSTGRESQL_URL")
	if !ok {
		panic("POSTGRESQL_URL not provided")
	}

	mqUri, ok := os.LookupEnv("KAFKA_URI")
	if !ok {
		panic("KAFKA_URI not provided")
	}

	return config{port, dbURI, mqUri}
}
