CREATE TABLE "rekening" (
  "id" varchar PRIMARY KEY,
  "saldo" numeric(12,3)
);

CREATE TABLE "harga" (
  "id" varchar PRIMARY KEY,
  "harga_topup" int,
  "harga_buyback" int,
  "created_by" varchar,
  "created_at" timestamp
);

CREATE TABLE "transaksi" (
  "id" varchar PRIMARY KEY,
  "type" varchar,
  "gram" numeric(12,3),
  "saldo" numeric(12,3),
  "harga_buyback" int,
  "harga_topup" int,
  "norek" varchar,
  "created_at" timestamp
);